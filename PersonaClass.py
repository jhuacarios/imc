class PersonaClass(object):
    nombre = ''
    edad = 0

    # femenino = 1, masculino = 2
    sexo = 0

    #para simplicidad se usaran numeros separados
    diaNAcimiento = 0
    mesNacimiento = 0
    anioNacimiento = 0

    peso = 0
    altura = 0

    # 1 = persona normal, 2 = atleta
    tipoPersona = 0

    def __init__(self, name):
        self.name = name

    def getNombre(self):
        return self.name

    def getIMC(self):
        return self.peso / (self.altura * self.altura)