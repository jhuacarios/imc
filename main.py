import os
import sys
from subprocess import call
from PersonaClass import PersonaClass

sys.path.append(".")


def cls():
    os.system('cls' if os.name=='nt' else 'clear')


menuPrincipal = 0
exitloop = 0
listaPersonas = []
nombreIngresado = 0
apellidoIngresado = 0

datovalidado = 0

# Funciones


def checkstring(test_str):
    import re
    pattern = r'[^\.a-zA-Z]'
    if re.search(pattern, test_str):
        return False
    else:
        return True


def validaredad(edadtest):
    if edadtest in range(15, 70):
        return True
    else:
        cls()
        print('Edad invalida (debe ser entre 15 y 70)')
        return False


def validaropciondual(opcion):
    if opcion == '1' or opcion == '2':
        return True
    else:
        return False


def validatename(string, tipodato):
    if checkstring(string):
        if len(string) > 30:
            cls()
            print('El ' + tipodato + ' no puede tener mas de 20 caracteres')
        else:
            return True
    else:
        cls()
        print('Solo puede ingresar letras y sin espacios')
        return False


cls()
while True:
    while True:
        print('Seleccione una alternativa (ingrese numero)')
        print('1.- Consultar por persona')
        menuPrincipal = raw_input('2.- Ingresar mis datos o los de otra persona\n')
        if validaropciondual(menuPrincipal):
            break
        else:
            cls()
            print('Opcion Invalida')

    cls()
    if menuPrincipal == '1':
        print('Ingrese el nombre')
        personaBuscar = input()
        continue

    if menuPrincipal == '2':
        while True:
            edad = raw_input('Ingrese su edad: ')
            if validaredad(edad):
                break

        while True:
            primerNombre = raw_input('Ingrese primer nombre: ')
            if validatename(primerNombre, 'nombre'):
                break

        while True:
            primerApellido = raw_input('Ingrese primer apellido: ')
            if validatename(primerApellido, 'apellido'):
                break

        while True:
            cls()
            print('Seleccione sexo (ingrese numero)')
            print('1.- Femenino')
            sexo = raw_input('2.- Masculino\n')
            if validaropciondual(sexo):
                break
            else:
                print('Opcion invalida')










